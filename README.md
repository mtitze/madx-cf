Combined-function magnet implementation in MAD-X
=====

FORTRAN implementation of formulas for symplectic tracking based on generating
functions applied to combined-functions (CFs) by Malte Titze.

* Windows
* Linux
* Mac OS X

Important note:
- The FORTRAN code is only for demonstration purposes and will not be maintained further.

Documentation
-------------

* https://github.com/mtitze/madx-cf/blob/master/cf_doc.pdf
* http://journals.aps.org/prab/pdf/10.1103/PhysRevAccelBeams.19.054002

Pre-requisites
--------------
Working FORTRAN compiler.
Official MAD-X v5.02.07 or v5.02.12

Downloading
-----------
### Latest (HEAD) ###

Via git:

    git clone https://github.com/mtitze/madx-cf

Installation
------------

    1) Locate trrun.f90 and twiss.f90 in your MAD-X folder
    and rename them to backup files.
    2) Link against the new .f90 files in this folder according to
       your MAD-X version.
    3) Compile MAD-X

License
-------

The IPython notebooks in this folder are distributed under the following
licence:

Copyright (c) 2015 by Malte Titze (malte.titze@cern.ch)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Contributors
------------
### [GitHub Contributor Graph](https://github.com/mtitze/madx-cf/graphs/contributors) ###

### Current Maintainers: ###
* Malte Titze

### Original Authors: ###
* Malte Titze


